# Contributors

A big thank you to all the contributors !

## Software

- [@twann](https://codeberg.org/twann)
- [@rokosun](https://codeberg.org/rokosun)
- [@schrmh](https://codeberg.org/schrmh)

## Website

- [@twann](https://codeberg.org/twann)
- [@joelchrono12](https://github.com/joelchrono12)


## Translations

- [@twann](https://codeberg.org/twann): French & German
- [@joelchrono12](https://github.com/joelchrono12): Spanish
- [@IngrownMink4](https://codeberg.org/IngrownMink4): Spanish
- [@mondstern](https://codeberg.org/mondstern): German & Dutch

## Other

And of course thank you to all the people who use this software and report bugs! :)
