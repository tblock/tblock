---

name: "Question"
about: "Ask your questions here"
title: "A clear and short title"
ref: "main"
labels:
- "Kind: Question"
- "Status: Stale"
- "Priority: Low"

---

### Before opening:
<!--
Before opening, please ensure that this issue is not duplicated.
-->

- [ ] This issue is not duplicated

### Question
<!--
Write you question here. Please be as clear as possible.
-->
