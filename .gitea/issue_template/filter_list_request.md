---

name: "Filter list request"
about: "Request a filter list to be added to our official repository index"
title: "A clear and short title"
ref: "main"
labels:
- "Related: Filter list repo"
- "Kind: Enhancement"
- "Status: Stale"
- "Priority: Low"

---

### Before opening:
<!--
Before opening, please ensure that this issue is not duplicated.
-->

- [ ] This issue is not duplicated

### Link to the filter lists you want to add
<!--
Please provide a link to both the plain text file AND its homepage
-->
- Title1: [source](#), [homepage](#)
- Title2: [source](#), [homepage](#)

### License check

- All the filter lists provided above are licensed under an open source license/public domain dedication: `yes/no/idk`
