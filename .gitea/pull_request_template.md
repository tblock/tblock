---

name: "Pull request"
about: "Open a new pull request"
title: "A clear and short title"
ref: "main"
labels:
- "Status: Stale"

---

<!--
Before opening, please ensure that this PR is not duplicated and that you have read 
both the contribution guidelines and the code of conduct.
Please also check that this PR is not duplicated.
-->

## Summary

<!--
Briefly describe the purpose of this pull request here. 
If it is meant to close an open issue, please also include link the issue here.
If this pull request aims to fix a problem, you can also provide a short description of the problem here.
-->

## Benefits

<!--
Why and how would this pull request useful? This section is meant to be a non-technical summary of the changes,
that can be read by users who don't have any knowledge of code. For example:

* Bug fixes (app will no longer crash)
* General UX improvements
-->

## Technical changes

<!--
List here everything that was changed in the code. For example:

* Deprecate function `module.example_function()`
* Remove redundant parenthesis in function `module.some_function()`
* Add new command-line option `--example-args`
-->

## Progression

<!--
If this pull request is still work-in-progress, you can include (and update over the time) the progression here. For example:

* [x] Add feature X
* [ ] Add feature Y
* [ ] Update unit tests
-->
