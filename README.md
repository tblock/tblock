<p align="center">
<img align="center" src="https://codeberg.org/tblock/tblock-gui/raw/commit/2456e9651103baef81f3a6c25cce97bf9d0252ee/assets/banner.png">
</p>
<p align="center">
<h1 align="center">TBlock</h1>
</p>
<p align="center">
<a href="https://github.com/humanetech-community/awesome-humane-tech"><img src="https://codeberg.org/tblock/tblock/raw/branch/main/extra/icons/humane-tech-badge.svg" alt="Awesome Humane Tech"></a>
<a href="https://framagit.org/twann/tblock/-/pipelines"><img src="https://framagit.org/twann/tblock/badges/main/pipeline.svg" alt="Pipeline status"></a>
<a href="https://nogithub.codeberg.page"><img src="https://nogithub.codeberg.page/badge.svg" alt="Please don't upload to GitHub"></a>
</p>

## Description

TBlock is a free and open-source system-wide ad-blocker that is compatible with most filter list formats.

To view the repository for the graphical user interface, [click here](https://codeberg.org/tblock/tblock-gui).

## Installation

You can easily install TBlock with:

```sh
pipx install tblock
```

More installation methods — such as Windows installer and packages for Linux distributions — can be found [on the website](https://tblock.codeberg.page/install).

## Usage

After installing, you need to setup TBlock in order to enable protection against ads and trackers:

```sh
tblock --init
```

You can learn more by reading [the documentation](https://docs.tblock.me/).

## Demo

[![asciicast](https://asciinema.org/a/589465.svg)](https://asciinema.org/a/589465)

## Roadmap

Here is a list of the upcoming changes in TBlock code. Suggestions are, of course, welcome.

- [x] Implement [a GUI for TBlock](https://codeberg.org/tblock/tblock-gui)
- [ ] Rewrite the entire code, make a clearer public API and publish release 3.0.0

## Contributing

Everyone is welcome to contribute to TBlock in different ways. Bug reports can be made directly from the [Gitea issue tracker](https://codeberg.org/tblock/tblock/issues), by sending an email to the team or in [our Matrix room](https://matrix.to/#/#tblock:envs.net). 

If you want to suggest a modification in the code, you can [open a pull request](https://codeberg.org/tblock/tblock/pulls) or send your patch using email or Matrix.

If you want to be a beta-tester, you can install the latest beta version from our [release page](https://codeberg.org/tblock/tblock/releases) and contact us to give a feedback.

You can find more information on how to contribute [here](https://codeberg.org/tblock/tblock/src/branch/main/CONTRIBUTING.md).

## Support

The best way to support is to [contribute](#contributing). If you want, you can also [make a donation](https://tblock.me/donate).

## Authors and acknowledgment

TBlock is currently mainained by [Twann](https://codeberg.org/twann), who is also its creator.
A big thank you to all the people [who contribute(d) to the project](https://codeberg.org/tblock/tblock/src/branch/main/CONTRIBUTORS.md).

## Credits

Here is a list of all libraries used in the project:

| Name | Author | License |
| --- | --- | --- |
| [colorama](https://github.com/tartley/colorama) | Jonathan Hartley | BSD |
| [requests](https://requests.readthedocs.io/) | Kenneth Reitz | Apache 2.0 |
| [urllib3](https://urllib3.readthedocs.io/) | Andrey Petrov | MIT |
| [argumentor](https://codeberg.org/twann/python-argumentor) | Twann | LGPLv3 |

It is also worth mentioning [pacman](https://archlinux.org/pacman/), from which TBlock's command-line design is highly inspired.

## Contact

- [Mastodon](https://fosstodon.org/@tblock)
- [Matrix](https://matrix.to/#/#tblock:matrix.org)
- [XMPP](xmpp:tblockproject@chat.disroot.org?join)

## License

[![GPLv3](https://www.gnu.org/graphics/gplv3-with-text-136x68.png)](https://www.gnu.org/licenses/gpl-3.0)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
