# Security Policy

This project takes security very seriously. If you found a vulnerability or any other issue related to security in this project's code, design or implementation, please follow the steps below to report it:

1. **Do not open a public issue**. It would allow people that did not know about the vulnerability in the first place to exploit it, thus putting all users in an even bigger danger.
2. Send a clear and concise description of the vulnerability (if possible, with examples of how it could be exploited and/or a solution about how it can be fixed) **in an encrypted email** addressed to `security <at> tblock <dot> me`. The GPG key used for encryption [is available here](https://keyoxide.org/hkp/d1a0535a384ba7a0deb87d7763180e50bc1ea89d).

**Note:** all development will be stopped until the issue is resolved since security issues have the highest priority.

---

_This document was last updated on January 4th, 2023._
